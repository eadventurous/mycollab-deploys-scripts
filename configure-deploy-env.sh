#!/bin/bash
sshpass -p "$SERVER_ROOT_PASS" ssh -o "StrictHostKeyChecking no" -tt "$SERVER_USER"@"$SERVER_IP" << !
	apt-get update -qq
	apt-get install -y -qq openjdk-8-jdk unzip < "/dev/null"
	exit 0
!
