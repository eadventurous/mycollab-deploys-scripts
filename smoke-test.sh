#!/bin/bash
HTTP_CODE="$(curl -o /dev/null --silent --head --write-out '%{http_code}\n' "$SERVER_IP":"$SERVER_PORT")"
echo "$HTTP_CODE"
if [ "$HTTP_CODE" == '200' ]; then
    exit 0
else
    exit 1
fi
