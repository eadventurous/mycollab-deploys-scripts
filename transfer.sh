#!/bin/bash
export SSHPASS="$SERVER_ROOT_PASS"
sshpass -e sftp -oBatchMode=no -o "StrictHostKeyChecking no" -b - "$SERVER_USER"@"$SERVER_IP" << !
   put artifacts.zip
   bye
!
