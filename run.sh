#!/bin/bash
sshpass -p "$SERVER_ROOT_PASS" ssh -o "StrictHostKeyChecking no" -tt "$SERVER_USER"@"$SERVER_IP" << !
	unzip artifacts.zip
	cd bin
	killall java
	chmod +x startup.sh
	./startup.sh </dev/null &>/dev/null &
	exit 0
!
