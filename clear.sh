#!/bin/bash
sshpass -p "$SERVER_ROOT_PASS" ssh -o "StrictHostKeyChecking no" -tt "$SERVER_USER"@"$SERVER_IP" << !
	rm -r ./*
	exit 0
!
echo $?
